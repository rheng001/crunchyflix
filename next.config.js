/** @type {import('next').NextConfig} */
const nextConfig = {
  distDir: "build",
  reactStrictMode: true,
  // typescript: {
  //   // !! WARN !!
  //   // Dangerously allow production builds to successfully complete even if
  //   // your project has type errors.
  //   // !! WARN !!
  //   ignoreBuildErrors: true,
  // },
  eslint: {
    // Warning: Dangerously allow production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true,
  },
  webpackDevMiddleware: (config) => {
    config.watchOptions = {
      poll: 800,
      aggregateTimeout: 300,
    };
    return config;
  },
  env: {
    TMDB_API_KEY: process.env.TMDB_API_KEY,
    APP_ENV: process.env.APP_ENV,
    API_STAGING: process.env.API_STAGING,
  },
  images: {
    domains: ["image.tmdb.org"],
  },
};

module.exports = nextConfig;
