import React from "react";
import Carousel from "react-material-ui-carousel";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";

interface CarouselItem {
  image: string;
}

interface Props {
  item: CarouselItem;
}

const Item = (props: Props) => {
  return (
    <Card elevation={0}>
      <CardContent>
        <CardMedia component="img" image={props.item.image} />
      </CardContent>
    </Card>
  );
};

const ImageCarousel = () => {
  var items = [
    {
      image:
        "https://www.themoviedb.org/t/p/w355_and_h200_multi_faces/lnEU5KKrpQNsDaxxjDHuzRRh8Zl.jpg",
    },
    {
      image:
        "https://www.themoviedb.org/t/p/w355_and_h200_multi_faces/qJxzjUjCpTPvDHldNnlbRC4OqEh.jpg",
    },
    {
      image:
        "https://www.themoviedb.org/t/p/w355_and_h200_multi_faces/m7FqiUOvsSk7Ulg2oRMfFGcLeT9.jpg",
    },
    {
      image:
        "https://www.themoviedb.org/t/p/w355_and_h200_multi_faces/mGVrXeIjyecj6TKmwPVpHlscEmw.jpg",
    },
    {
      image:
        "https://www.themoviedb.org/t/p/w355_and_h200_multi_faces/ewUqXnwiRLhgmGhuksOdLgh49Ch.jpg",
    },
  ];

  return (
    <Box sx={{ width: "100%", height: "100%" }}>
      <Carousel indicators navButtonsAlwaysVisible>
        {items.map((item, i) => (
          <Item key={i} item={item} />
        ))}
      </Carousel>
    </Box>
  );
};

export default ImageCarousel;
