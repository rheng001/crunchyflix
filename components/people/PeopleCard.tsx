import React from "react";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";

import empty from "@/public/images/empty.svg";

interface Person {
  profile_path: string;
  name: string;
}

interface PeopleCardProps {
  person: Person;
}

const PeopleCard = (
  {
    person
  }: PeopleCardProps
) => {
  const imagePath =
    `https://image.tmdb.org/t/p/w227_and_h127_bestv2` + person.profile_path;

  return (
    <Grid item xs={4}>
      <Card>
        <CardMedia
          src={person.profile_path !== null ? imagePath : empty.src}
          component="img"
          width="200"
          height="200"
          sx={{ objectFit: "contain" }}
        />
        <CardContent>
          <Typography sx={{ textAlign: "center" }}>{person.name}</Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default PeopleCard;
