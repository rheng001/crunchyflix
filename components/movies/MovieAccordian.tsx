import React, { useState } from "react";
import { useQuery } from "react-query";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Grid from "@mui/material/Grid";

import MovieMediaCard from "@/components/movies/MovieMediaCard";
import { fetchMovieMedia } from "@/api/api";

interface Movie {
  id: number;
  name: string;
  original_title: string;
  still_path: string;
  type: string;
  key: string;
}

interface MovieAccordianProps {
  movieDetails: Movie;
}

const MovieAccordian = (
  {
    movieDetails
  }: MovieAccordianProps
) => {
  const [expanded, setExpanded] = useState(false);

  const {
    data: movieLoaded,
    refetch,
    isLoading,
    isError,
    isPreviousData,
    isSuccess,
    error,
  } = useQuery(["movie", movieDetails.id], fetchMovieMedia, {
    keepPreviousData: true,
  });

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }

  return (
    <Accordion
      onChange={(e, expanded) => {
        if (expanded) {
          refetch();
          setExpanded(true);
        } else {
          setExpanded(false);
        }
      }}
      sx={{ minWidth: "740px" }}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
        sx={{
          backgroundColor: "#e5e5e5",
        }}
      >
        <Typography
          sx={{ color: expanded ? "#3f3f3f" : "#0a6da4", fontWeight: "bold" }}
        >{`${movieDetails.original_title} `}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Grid container direction="row" spacing={5} xs={12} md={12}>
          {movieLoaded.results.map((movie: Movie) => {
            return <MovieMediaCard movie={movie} />;
          })}
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
};

export default MovieAccordian;
