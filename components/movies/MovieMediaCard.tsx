import React, { useState } from "react";
import ReactPlayer from "react-player";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import { CardActionArea } from "@mui/material";
import Tooltip, { TooltipProps, tooltipClasses } from "@mui/material/Tooltip";
import { styled } from "@mui/material/styles";
import Modal from "@mui/material/Modal";

import empty from "@/public/images/empty.svg";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  bgcolor: "#000",
  boxShadow: 24,
  p: 0.3,
  outline: 0,
};

const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: "#3f3f3f",
    color: "#FFFFFF",
    boxShadow: theme.shadows[1],
    fontSize: 12,
  },
}));

interface Movie {
  id: number;
  name: string;
  original_title: string;
  still_path: string;
  type: string;
  key: string;
}

interface MovieMediaProps {
  movie: Movie;
}

const MovieMediaCard = (
  {
    movie
  }: MovieMediaProps
) => {
  const imagePath = `https://i.ytimg.com/vi/` + movie.key + `/hqdefault.jpg`;

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Grid item xs={3} md={3}>
        <LightTooltip title={movie.name} placement="right" arrow>
          <Card
            sx={{
              minWidth: 150,
            }}
            variant="outlined"
            onClick={handleOpen}
          >
            <CardActionArea
              sx={{
                minWidth: 150,
              }}
            >
              <CardContent>
                <CardMedia
                  component="img"
                  width="135"
                  height="70"
                  image={movie.still_path !== null ? imagePath : empty.src}
                />
                <Typography variant="subtitle2" component="h1" mt={2} noWrap>
                  {movie.name}
                </Typography>
                <Typography
                  variant="subtitle1"
                  component="h2"
                  color={"#757575"}
                  overflow="hidden"
                  noWrap
                >
                  {movie.type}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </LightTooltip>
      </Grid>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <ReactPlayer
            url={`https://www.youtube.com/watch?v=${movie.key}`}
            controls
          />
        </Box>
      </Modal>
    </>
  );
};

export default MovieMediaCard;
