import React, { useState, useEffect } from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Rating from "@mui/material/Rating";
import Divider from "@mui/material/Divider";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";

import empty from "@/public/images/empty.svg";

interface Network {
  name: string;
}

interface Genre {
  id: number;
  name: string;
}

interface Movie {
  poster_path: string;
  overview: string;
  vote_average: number;
  networks: Array<Network>;
  genres: Array<Genre>;
}

interface MovieDetailsProps {
  movieDetails: Movie;
}

const MovieDetails = (
  {
    movieDetails
  }: MovieDetailsProps
) => {
  const [showMore, setShowMore] = useState(false);

  const imagePath =
    `https://image.tmdb.org/t/p/w300_and_h450_bestv2` +
    movieDetails.poster_path;

  const convertRating = (rating: number) => {
    return (rating / 10) * 5;
  };

  const trimText =
    movieDetails.overview && movieDetails.overview.substring(0, 100);

  return (
    <Container maxWidth="sm">
      <img
        src={movieDetails.poster_path !== null ? imagePath : empty.src}
        width="300px"
        height="450px"
        style={{ objectFit: "cover" }}
      />

      <Divider />

      <Typography variant="subtitle2" component="h1" sx={{ marginTop: 2 }}>
        About the Movie
      </Typography>
      <Box component="span" sx={{ paddingTop: 2, paddingBottom: 5 }}>
        <Typography variant="body1">
          {showMore && movieDetails.overview !== undefined
            ? movieDetails.overview
            : trimText}
          <Button variant="text" onClick={() => setShowMore(!showMore)}>
            {showMore ? "Show Less" : "Show more"}
          </Button>
        </Typography>
      </Box>

      <Divider />

      <Typography variant="subtitle2" component="h1" pt={2}>
        User Ratings
      </Typography>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          paddingY: 1,
          alignItems: "center",
        }}
      >
        <Typography variant="subtitle2" component="h1">
          Average Rating :{" "}
        </Typography>
        <Rating
          name="read-only"
          value={convertRating(movieDetails.vote_average)}
          readOnly
          precision={0.25}
        />
      </Box>

      <Divider />

      <Typography variant="subtitle2" component="h1" pt={2}>
        Details
      </Typography>

      <Typography variant="subtitle2" component="h1" pt={2} color="gray">
        Publisher:
        <Box component={"span"} sx={{ marginLeft: 1, mx: 1 }}>
          {movieDetails.networks &&
            movieDetails.networks.map((network) => {
              return (
                <Typography variant="caption" sx={{ color: "#000" }}>
                  {`${network.name}`}
                </Typography>
              );
            })}
        </Box>
      </Typography>

      <Typography variant="subtitle2" component="h1" color="gray">
        Tags:
        <Box component={"span"} sx={{ marginLeft: 1, mx: 1 }}>
          {movieDetails.genres &&
            movieDetails.genres.map((genre, index) => {
              return (
                <Typography
                  variant="caption"
                  key={genre.id}
                  sx={{ color: "#000" }}
                >
                  {genre.name}
                  {index < movieDetails.genres.length - 1 ? ", " : ""}
                </Typography>
              );
            })}
        </Box>
      </Typography>
    </Container>
  );
};

export default MovieDetails;
