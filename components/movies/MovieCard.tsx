import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import { CardActionArea } from "@mui/material";
import Link from "@mui/material/Link";

import empty from "@/public/images/empty.svg";

interface Movie {
  id: number;
  poster_path: string;
  title: string;
  overview: string;
}

interface MovieCardProps {
  movie: Movie;
}

const MovieCard = ({ movie }: MovieCardProps) => {
  const imagePath =
    `https://image.tmdb.org/t/p/w220_and_h330_face` + movie.poster_path;

  return (
    <Grid item xs={4} sm={4} md={3}>
      <Link href={`/movie/${movie.id}`}>
        <Card
          sx={{
            width: "151px",
            height: "280px",
          }}
        >
          <CardActionArea>
            <CardContent>
              <CardMedia
                component="img"
                image={movie.poster_path !== null ? imagePath : empty.src}
                sx={{
                  width: "125px",
                  height: "187px",
                }}
              />
              <Typography variant="subtitle2" component="h1" mt={2} noWrap>
                {movie.title}
              </Typography>
              <Typography
                variant="subtitle1"
                component="h2"
                color={"#757575"}
                overflow="hidden"
                noWrap
              >
                {movie.overview}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </Link>
    </Grid>
  );
};

export default MovieCard;
