import React, { useState, useEffect } from "react";
import { useQuery } from "react-query";
import { isValid, format, parseISO } from "date-fns";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Avatar from "@mui/material/Avatar";
import Rating from "@mui/material/Rating";

import { fetchReviews } from "@/api/api";

interface ReviewCardProps {
  id: number | string | undefined | string[];
  type: string;
}

interface Author {
  rating: number;
  avatar_path: string;
}

interface Review {
  author: string;
  content: string;
  created_at: string;
  author_details: Author;
}

const ReviewCard = (
  {
    id,
    type
  }: ReviewCardProps
) => {
  const {
    data: reviews,
    isLoading,
    isError,
    isPreviousData,
    isSuccess,
    error,
  } = useQuery(["review", type, id], fetchReviews, { keepPreviousData: true });

  const convertRating = (rating: number) => {
    return (rating / 10) * 5;
  };

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }

  return (
    <Grid spacing={0}>
      <Typography variant="h6">User Reviews</Typography>
      {reviews.results &&
        reviews.results.map((review: Review) => {
          return (
            <Grid
              item
              md={12}
              spacing={1}
              sx={{
                marginY: 5,
              }}
            >
              <Paper
                elevation={3}
                sx={{
                  padding: 5,
                }}
              >
                <Rating
                  name="read-only"
                  value={convertRating(review.author_details.rating)}
                  readOnly
                  precision={0.25}
                />
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    my: 2,
                  }}
                >
                  <Avatar
                    alt={review.author}
                    src={
                      `https://image.tmdb.org/t/p/w220_and_h330_face` +
                      review.author_details.avatar_path
                    }
                    sx={{
                      marginRight: "15px",
                    }}
                  />

                  <Typography fontWeight={"bold"}>
                    by {review.author} on{" "}
                    {format(parseISO(review.created_at), "PPP")}
                  </Typography>
                </Box>
                <Typography mt={5}>{review.content}</Typography>
              </Paper>
            </Grid>
          );
        })}
    </Grid>
  );
};

export default ReviewCard;
