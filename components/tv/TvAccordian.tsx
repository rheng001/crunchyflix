import React, { useState } from "react";
import { useQuery } from "react-query";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Grid from "@mui/material/Grid";

import EpisodeCard from "@/components/tv/EpisodeCard";
import { fetchTVSeasons } from "@/api/api";

interface Season {
  season_number: number;
}

interface ShowDetails {
  id: number;
  name: string;
}

interface Episode {
  still_path: string;
  episode_number: string;
  name: string;
  overview: string;
}

interface TvAccordianProps {
  season: Season;
  showDetails: ShowDetails;
  sortValue: number;
}

const TvAccordian = ({ season, showDetails, sortValue }: TvAccordianProps) => {
  const [expanded, setExpanded] = useState(false);

  const {
    data: seasonLoaded,
    refetch,
    isLoading,
    isError,
    isPreviousData,
    isSuccess,
    error,
  } = useQuery(
    ["tv_seasons", showDetails.id, season.season_number],
    fetchTVSeasons,
    { keepPreviousData: true }
  );

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }

  return (
    <Accordion
      onChange={(e, expanded) => {
        if (expanded) {
          refetch();
          setExpanded(true);
        } else {
          setExpanded(false);
        }
      }}
      sx={{ minWidth: "740px" }}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
        sx={{
          backgroundColor: "#e5e5e5",
        }}
      >
        <Typography
          sx={{ color: expanded ? "#3f3f3f" : "#0a6da4", fontWeight: "bold" }}
        >{`${showDetails.name} Season ${season.season_number}`}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Grid container spacing={5} md={12}>
          {seasonLoaded.episodes &&
            seasonLoaded.episodes
              .sort((a: any, b: any) =>
                sortValue === 0
                  ? b.episode_number - a.episode_number
                  : a.episode_number - b.episode_number
              )
              .map((episode: Episode) => {
                return <EpisodeCard episode={episode} />;
              })}
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
};

export default TvAccordian;
