import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import { CardActionArea } from "@mui/material";
import Tooltip, { TooltipProps, tooltipClasses } from "@mui/material/Tooltip";
import { styled } from "@mui/material/styles";

import empty from "@/public/images/empty.svg";

const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: "#3f3f3f",
    color: "#FFFFFF",
    boxShadow: theme.shadows[1],
    fontSize: 12,
  },
}));

interface Episode {
  still_path: string;
  episode_number: string;
  name: string;
  overview: string;
}

interface EpisodeCardProps {
  episode: Episode;
}

const EpisodeCard = (
  {
    episode
  }: EpisodeCardProps
) => {
  const imagePath =
    `https://image.tmdb.org/t/p/w227_and_h127_bestv2` + episode.still_path;

  return (
    <Grid item xs={3} md={3}>
      <LightTooltip title={episode.overview} placement="right" arrow>
        <Card
          sx={{
            minWidth: 150,
          }}
          variant="outlined"
        >
          <CardActionArea
            sx={{
              minWidth: 150,
            }}
          >
            <CardContent>
              <CardMedia
                component="img"
                width="135"
                height="70"
                image={episode.still_path !== null ? imagePath : empty.src}
              />
              <Typography variant="subtitle2" component="h1" mt={2}>
                Episode {episode.episode_number}
              </Typography>
              <Typography
                variant="subtitle1"
                component="h2"
                color={"#757575"}
                overflow="hidden"
                noWrap
              >
                {episode.name}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </LightTooltip>
    </Grid>
  );
};

export default EpisodeCard;
