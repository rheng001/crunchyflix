import React, { useState } from "react";
import NextImage from "next/image";
import { useQuery } from "react-query";
import { styled, alpha } from "@mui/material/styles";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Link from "@mui/material/Link";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";

import { fetchSearchTerm } from "@/api/api";
import Searchbar from "@/components/Searchbar";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  // backgroundColor: alpha(theme.palette.common.white, 0.15),
  backgroundColor: "#FFF",
  "&:hover": {
    // backgroundColor: alpha(theme.palette.common.white, 0.25),
    backgroundColor: "#FFF",
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  right: 0,
  top: 0,
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  "&:hover": {
    color: "#F23",
    backgroundColor: "#F23",
  },
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(0.5)})`,
    paddingRight: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

const NavBar = () => {
  const [inputValue, setInputValue] = useState<string>("Batman");

  const { data, isLoading, isError } = useQuery(
    ["search_term", inputValue],
    fetchSearchTerm,
    {
      enabled: Boolean(inputValue),
    }
  );

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" sx={{ backgroundColor: "#000" }}>
        <Container>
          <Toolbar disableGutters>
            <Link href="/">
              <NextImage
                src="/images/logo.png"
                layout="fixed"
                priority
                width={135}
                height={26}
                quality={100}
              />
            </Link>
            <Stack
              direction={"row"}
              spacing={5}
              sx={{
                flexGrow: 1,
                display: "flex",
                marginX: 5,
                justifyContent: "flex-start",
              }}
            >
              <Link color={"#F23"} underline="hover" href="/tv" noWrap>
                TV Shows
              </Link>
              <Link color={"#F23"} underline="hover" href="/movie" noWrap>
                Movies
              </Link>
              <Link color={"#F23"} underline="hover" href="/people" noWrap>
                People
              </Link>
            </Stack>

            <Box sx={{ flexGrow: 0, display: "flex" }}>
              <Search>
                {/* <StyledInputBase
                  placeholder="TV, movie, etc."
                  inputProps={{ "aria-label": "search" }}
                  value={inputValue}
                  onChange={(event) => {
                    setInputValue(event.target.value);
                  }}
                /> */}
                <Searchbar />

                <SearchIconWrapper>
                  <SearchIcon color="secondary" />
                </SearchIconWrapper>
              </Search>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </Box>
  );
};

export default NavBar;
