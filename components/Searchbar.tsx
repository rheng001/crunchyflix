import React, { useState, useEffect } from "react";
import { useInView } from "react-intersection-observer";
import { useQuery, useInfiniteQuery } from "react-query";
import TextField from "@mui/material/TextField";
import { styled, alpha } from "@mui/material/styles";
import Autocomplete from "@mui/material/Autocomplete";
import InputBase from "@mui/material/InputBase";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

import empty from "@/public/images/empty.svg";
import { fetchSearchTerm } from "@/api/api";

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(0.5)})`,
    paddingRight: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}));

const Searchbar = () => {
  const [inputValue, setInputValue] = useState("");
  const [page, setPage] = useState(1);
  const { ref, inView } = useInView();

  const { data, isLoading, isError } = useQuery(
    ["search_term", inputValue, page],
    fetchSearchTerm,
    {
      keepPreviousData: true,
      enabled: Boolean(inputValue),
    }
  );

  useEffect(() => {
    if (inView) {
      setPage((old) => old + 1);
    }
  }, [inView]);

  const imagePath = `https://image.tmdb.org/t/p/w220_and_h330_face`;

  return (
    <Box>
      <Autocomplete
        id="combo-box-demo"
        popupIcon={""}
        getOptionLabel={(option: { title: string }) => option.title}
        options={data?.results || []}
        sx={{ width: 300 }}
        isOptionEqualToValue={(option, value) => option.title === value.title}
        inputValue={inputValue}
        onInputChange={(event, newInputValue) => {
          setInputValue(newInputValue);
        }}
        noOptionsText={"TV, movie, etc."}
        renderInput={(params) => (
          <TextField
            {...params}
            label="TV, movie, etc."
            variant="filled"
            color="secondary"
          />
        )}
        renderOption={(props, jsonResults: any) => (
          <Link
            href={`/${jsonResults.media_type}/${jsonResults.id}`}
            underline="none"
            sx={{ color: "#000" }}
          >
            <Box component="li" {...props}>
              <Box sx={{ minWidth: "42px", minHeight: "24px" }}>
                <img
                  src={
                    jsonResults.poster_path !== null
                      ? imagePath + jsonResults.poster_path
                      : empty.src
                  }
                  width="42px"
                  height="24px"
                  style={{ objectFit: "cover" }}
                />
              </Box>

              <Box sx={{ px: 2 }}>
                <Typography
                  noWrap
                  sx={{
                    fontWeight: "bold",
                  }}
                >
                  {jsonResults.media_type === "tv"
                    ? jsonResults.name
                    : jsonResults.title}
                </Typography>
                <Typography component="span">
                  {jsonResults.media_type}
                </Typography>
              </Box>
              <span ref={ref} style={{ visibility: "hidden" }}>
                intersection observe marker
              </span>
            </Box>
          </Link>
        )}
      />
    </Box>
  );
};

export default Searchbar;
