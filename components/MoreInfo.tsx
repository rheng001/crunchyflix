import React from "react";
import { useQuery } from "react-query";
import Grid from "@mui/material/Grid";

import CastCard from "@/components/CastCard";
import { fetchCast } from "@/api/api";

interface MoreInfoProps {
  id: number | string | undefined | string[];
  type: string;
}

interface Role {
  character: string;
}

interface Cast {
  profile_path: string;
  name: string;
  character: string;
  roles: Array<Role>;
}

const MoreInfo = (
  {
    id,
    type
  }: MoreInfoProps
) => {
  const { data, isError, isLoading } = useQuery(["cast", id, type], fetchCast);

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }
  return (
    <Grid container xs={12} md={12} spacing={5}>
      {data.cast.map((cast: Cast) => {
        return <CastCard cast={cast} type={type} />;
      })}
    </Grid>
  );
};

export default MoreInfo;
