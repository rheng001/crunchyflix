import React from "react";
import { useQuery } from "react-query";
import Stack from "@mui/material/Stack";

import NowShowingCard from "@/components/home/NowShowingCard";
import { fetchAiringTodayShows } from "@/api/api";

interface Latest {
  id: number;
  name: string;
  poster_path: string;
  vote_average: string;
}

const NowShowingSection = () => {
  const {
    data: latestTV,
    isLoading,
    isError,
  } = useQuery("now_showing", fetchAiringTodayShows);

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }
  return (
    <Stack spacing={0.5} sx={{ paddingTop: 2 }}>
      {latestTV.results.slice(0, 5).map((latest: Latest) => {
        return (
          <React.Fragment key={latest.id}>
            <NowShowingCard media={latest} type="movies" />
          </React.Fragment>
        );
      })}
    </Stack>
  );
};

export default NowShowingSection;
