import React from "react";
import { useQuery } from "react-query";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

import FeaturedCard from "@/components/home/FeaturedCard";

import { fetchLatestMovies } from "@/api/api";

const FeaturedSection = () => {
  const {
    data: latestMovies,
    isLoading,
    isError,
  } = useQuery("featured", fetchLatestMovies);

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }
  return (
    <Box>
      <Typography
        variant="subtitle2"
        component="h1"
        sx={{ fontWeight: "bold", paddingY: 2 }}
      >
        Featured Shows
      </Typography>
      {latestMovies.results.slice(0, 5).map((latest: any) => {
        return (
          <React.Fragment key={latest.id}>
            <FeaturedCard media={latest} type="movies" />
          </React.Fragment>
        );
      })}
    </Box>
  );
};

export default FeaturedSection;
