import React from "react";
import { useQuery, UseQueryResult } from "react-query";
import Box from "@mui/material/Box";

import LatestCard from "@/components/home/LatestCard";

import { fetchNowPlayingMovies } from "@/api/api";

interface Latest {
  id: number;
  poster_path: string;
  title: string;
  overview: string;
}

const LatestSection = () => {
  const {
    data: latestMovies,
    isLoading,
    isError,
  } = useQuery("latest", fetchNowPlayingMovies);

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }
  return (
    <Box sx={{ pt: 2 }}>
      {latestMovies.results.slice(0, 6).map((latest: Latest) => {
        return (
          <React.Fragment key={latest.id}>
            <LatestCard media={latest} type="movies" />
          </React.Fragment>
        );
      })}
    </Box>
  );
};

export default LatestSection;
