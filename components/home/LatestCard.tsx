import React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

import empty from "@/public/images/empty.svg";

interface Media {
  id: number;
  poster_path: string;
  title: string;
  overview: string;
}
interface LatestCardProps {
  media: Media;
  type: string;
}

const LatestCard = (
  {
    media,
    type
  }: LatestCardProps
) => {
  const imagePath =
    `https://image.tmdb.org/t/p/w220_and_h330_face` + media.poster_path;

  return (
    <Box
      sx={{
        maxHeight: "162px",
        marginBottom: 3,
      }}
    >
      <Paper elevation={0}>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-start",
          }}
        >
          <Box sx={{ minWidth: "162px", minHeight: "162px" }}>
            <Link href={`/movie/${media.id}`}>
              <img
                src={media.poster_path !== null ? imagePath : empty.src}
                width="162px"
                height="162px"
                style={{
                  objectFit: "cover",
                }}
              />
            </Link>
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              m: 2,
              justifyContent: "space-between",
            }}
          >
            <Typography sx={{ fontWeight: "bold" }} component="h1">
              {media.title}
            </Typography>
            <Typography variant="body1" component="p">
              {media.overview.substring(0, 100) + `...`}
            </Typography>
            <Link
              href={`/movie/${media.id}`}
              underline={"hover"}
              color="#E50914"
            >
              <Typography sx={{ textAlign: "right", color: "#E50914" }}>
                {"view more »"}
              </Typography>
            </Link>
          </Box>
        </Box>
      </Paper>
    </Box>
  );
};

export default LatestCard;
