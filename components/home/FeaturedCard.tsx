import React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

import empty from "@/public/images/empty.svg";

interface Media {
  id: number;
  poster_path: string;
  name: string;
  vote_average: string;
}

interface FeaturedCardProps {
  media: Media;
  type: string;
}

const FeaturedCard = ({ media, type }: FeaturedCardProps) => {
  const imagePath =
    `https://image.tmdb.org/t/p/w220_and_h330_face` + media.poster_path;

  return (
    <Box
      sx={{
        maxHeight: "75px",
        marginBottom: 3,
        width: "296px",
      }}
    >
      <Link href={`/tv/${media.id}`}>
        <Paper elevation={0}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-start",
            }}
          >
            <img
              src={media.poster_path !== null ? imagePath : empty.src}
              width="50px"
              height="75px"
              style={{
                objectFit: "cover",
              }}
            />

            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                m: 2,
                justifyContent: "flex-start",
              }}
            >
              <Typography
                sx={{ fontWeight: "bold" }}
                variant="subtitle2"
                component="h1"
              >
                {media.name}
              </Typography>
              <Typography variant="caption" component="p">
                Rating: {media.vote_average}
              </Typography>
            </Box>
          </Box>
        </Paper>
      </Link>
    </Box>
  );
};

export default FeaturedCard;
