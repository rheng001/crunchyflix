import React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

import empty from "@/public/images/empty.svg";

interface Media {
  id: number;
  name: string;
  poster_path: string;
  vote_average: string;
}

interface NowShowingCardProps {
  media: Media;
  type: string;
}

const NowShowingCard = ({ media, type }: NowShowingCardProps) => {
  const imagePath =
    `https://image.tmdb.org/t/p/w100_and_h100_face` + media.poster_path;

  return (
    <Box>
      <Link href={`/tv/${media.id}`}>
        <Paper elevation={0}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-start",
            }}
          >
            <Box sx={{ minWidth: "100px", minHeight: "56px" }}>
              <img src={media.poster_path !== null ? imagePath : empty.src} />
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                m: 2,
                justifyContent: "flex-start",
              }}
            >
              <Typography
                sx={{ fontWeight: "medium" }}
                variant="caption"
                component="h1"
              >
                {media.name}
              </Typography>
              <Typography variant="caption" component="p">
                Rating: {media.vote_average}
              </Typography>
            </Box>
          </Box>
        </Paper>
      </Link>
    </Box>
  );
};

export default NowShowingCard;
