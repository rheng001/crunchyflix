import React from "react";
import NextImage from "next/image";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import { CardActionArea } from "@mui/material";

import empty from "@/public/images/empty.svg";

interface Role {
  character: string;
}

interface Cast {
  profile_path: string;
  name: string;
  character: string;
  roles: Array<Role>;
}

interface Props {
  type: string;
  cast: Cast;
}

const CastCard = ({ cast, type }: Props) => {
  const imagePath =
    `https://image.tmdb.org/t/p/w220_and_h330_face` + cast.profile_path;

  return (
    <Grid item xs={3}>
      <Card
        sx={{
          minWidth: 150,
        }}
        variant="outlined"
      >
        <CardActionArea
          sx={{
            minWidth: 150,
          }}
        >
          <CardContent
            sx={{
              paddingBottom: "0%",
              maxHeight: "300px",
              minHeight: "300px",
              overflow: "auto",
            }}
          >
            <CardMedia>
              <Box
                sx={{
                  position: "relative",
                  minHeight: "150px",
                  maxHeight: "150px",
                }}
              >
                <NextImage
                  src={cast.profile_path !== null ? imagePath : empty.src}
                  layout="fill"
                  objectFit="cover"
                  priority
                />
              </Box>
            </CardMedia>
            <Typography
              variant="subtitle2"
              component="h1"
              mt={2}
              sx={{ textAlign: "left" }}
            >
              {cast.name}
            </Typography>
            <Typography
              variant="subtitle2"
              component="h1"
              mt={1}
              sx={{ textAlign: "left" }}
            >
              {type === "tv" ? cast.roles[0].character : cast.character}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
};

export default CastCard;
