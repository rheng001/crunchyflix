import Container from "@mui/material/Container";
import Box from "@mui/material/Box";

import NavBar from "@/components/NavBar";
import { ReactNode, ReactElement } from "react";

const Layout = ({ children }: { children: ReactNode }): ReactElement => {
  return (
    <Box sx={{}}>
      <NavBar />
      <Container sx={{ marginTop: 5, marginBottom: 10 }}>{children}</Container>
    </Box>
  );
};

export default Layout;
