import React from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Layout from "@/components/Layout";
import Divider from "@mui/material/Divider";

import LatestSection from "@/components/home/LatestSection";
import FeaturedSection from "@/components/home/FeaturedSection";
import NowShowingSection from "@/components/home/NowShowingSection";
import Carousel from "@/components/Carousel";

const Index = () => {
  return (
    <Layout>
      <Stack
        direction={{ xs: "column", md: "row", lg: "row" }}
        spacing={5}
        sx={{
          backgroundColor: "#FFF",
          p: 2,
        }}
        divider={<Divider orientation="vertical" flexItem />}
      >
        <Stack
          direction="column"
          spacing={2}
          sx={{
            maxWidth: "600px",
          }}
        >
          <Box
            sx={{
              width: "600px",
              height: "400px",
            }}
          >
            <Carousel />
          </Box>

          <Box>
            <Typography variant="h6" component="h1">
              Latest Movies
            </Typography>
            <Divider />
            <LatestSection />
          </Box>
        </Stack>
        <Box>
          <img
            src={
              "https://www.themoviedb.org/t/p/w355_and_h200_multi_faces/4OTYefcAlaShn6TGVK33UxLW9R7.jpg"
            }
            width="420px"
            height="250px"
            style={{
              objectFit: "cover",
            }}
          />

          <Stack
            direction="column"
            spacing={2}
            sx={{
              maxWidth: "500px",
            }}
          >
            <Box>
              <FeaturedSection />
              <Divider />
              <Typography
                variant="h6"
                component="h1"
                sx={{ fontWeight: "bold", pt: 2 }}
              >
                Now Showing
              </Typography>
              <Divider />
              <NowShowingSection />
            </Box>
          </Stack>
        </Box>
      </Stack>
    </Layout>
  );
};

export default Index;
