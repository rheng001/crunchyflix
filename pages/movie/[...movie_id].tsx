import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { useRouter } from "next/router";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";

import Layout from "@/components/Layout";
import MovieDetails from "@/components/movies/MovieDetails";
import MovieAccordian from "@/components/movies/MovieAccordian";
import ReviewCard from "@/components/ReviewCard";
import MoreInfo from "@/components/MoreInfo";
import { fetchMovieDetails } from "@/api/api";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

const MovieInfoPage = () => {
  const router = useRouter();
  const { movie_id } = router.query;

  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const {
    data: movieDetails,
    isSuccess,
    isLoading,
    isError,
  } = useQuery(["movie", movie_id], fetchMovieDetails, {
    enabled: movie_id !== undefined,
  });

  const TabPanel = (props: TabPanelProps) => {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  };

  const a11yProps = (index: number) => {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  };

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }

  if (isSuccess) {
    return (
      <Layout>
        <Typography variant="h6" component="h1" sx={{ py: 2 }}>
          {movieDetails.title}
        </Typography>

        <Stack
          direction="row"
          spacing={2}
          sx={{
            display: "flex",
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Stack direction="column" spacing={2}>
            <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
              <Tabs
                sx={{ backgroundColor: "#555" }}
                value={value}
                onChange={handleChange}
                indicatorColor="secondary"
                textColor="secondary"
              >
                <Tab label="Videos" {...a11yProps(0)} />
                <Tab label="Reviews" {...a11yProps(1)} />
                <Tab label="Discussions" {...a11yProps(2)} />
                <Tab label="More Info" {...a11yProps(3)} />
              </Tabs>
            </Box>
            <TabPanel value={value} index={0}>
              <Box sx={{ marginY: 2 }}>
                <MovieAccordian movieDetails={movieDetails} />
              </Box>
            </TabPanel>
            <TabPanel value={value} index={1}>
              <Box
                sx={{
                  marginY: 2,
                  width: "750px",
                  minWidth: "100%",
                }}
              >
                <ReviewCard id={movie_id} type="movie" />
              </Box>
            </TabPanel>
            <TabPanel value={value} index={2}>
              <Box
                sx={{
                  marginY: 2,
                  width: "750px",
                  minWidth: "100%",
                }}
              >
                <Typography variant="h6">Discussions</Typography>
              </Box>
            </TabPanel>
            <TabPanel value={value} index={3}>
              <Box sx={{ minWidth: "740px" }}>
                <MoreInfo id={movie_id} type="movie" />
              </Box>
            </TabPanel>
          </Stack>

          <Box sx={{}}>
            <MovieDetails movieDetails={movieDetails} />
          </Box>
        </Stack>
      </Layout>
    );
  }
  return <></>;
};

export default MovieInfoPage;
