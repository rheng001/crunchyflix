import React, { useState, useEffect } from "react";
import { useQuery, useInfiniteQuery } from "react-query";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import Divider from "@mui/material/Divider";

import Layout from "@/components/Layout";
import MovieCard from "@/components/movies/MovieCard";
import FeaturedSection from "@/components/home/FeaturedSection";
import { fetchNowPlayingMovies } from "@/api/api";

interface LinkTabProps {
  label?: string;
  href?: string;
}

interface Movie {
  id: number;
  poster_path: string;
  title: string;
  overview: string;
}

const LinkTab = (props: LinkTabProps) => {
  return (
    <Tab
      component="a"
      onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        // event.preventDefault();
      }}
      {...props}
    />
  );
};

const Movies = () => {
  const [value, setValue] = useState<number>(1);
  const [value2, setValue2] = useState<number>(1);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const handleChange2 = (event: React.SyntheticEvent, newValue: number) => {
    setValue2(newValue);
  };

  const {
    data: movies,
    isLoading,
    isError,
    isPreviousData,
    isSuccess,
    error,
    isFetching,
    fetchNextPage,
    isFetchingNextPage,
  } = useInfiniteQuery(["movies_now_playing"], fetchNowPlayingMovies, {
    keepPreviousData: true,
    getNextPageParam: (lastPage, allPages) => {
      const maxPages = lastPage.total_pages;
      const nextPage = allPages.length + 1;
      return nextPage <= maxPages ? nextPage : undefined;
    },
  });

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }

  return (
    <Layout>
      <Box sx={{ marginBottom: 2 }}>
        <Tabs
          sx={{ backgroundColor: "#555" }}
          value={value}
          onChange={handleChange}
          indicatorColor="secondary"
          textColor="secondary"
        >
          <LinkTab label="TV Shows" href="/tv" />
          <LinkTab label="Movies" href="/movie" />
        </Tabs>
        <Container sx={{ backgroundColor: "#FFF" }}>
          <Tabs
            value={value2}
            onChange={handleChange2}
            indicatorColor="secondary"
            textColor="secondary"
          >
            <LinkTab label="Popular" href="/movie" />
            <LinkTab label="Now Playing" href="/movie/now-playing" />
            <LinkTab label="Upcoming" href="/movie/upcoming" />
            <LinkTab label="Top Rated" href="/movie/top-rated" />
          </Tabs>
        </Container>
      </Box>
      <Stack direction="row" spacing={5}>
        <Grid container spacing={2} md={7}>
          {movies?.pages.map((group, i) =>
            group.results.map((movie: Movie) => {
              return (
                <React.Fragment key={movie.id}>
                  <MovieCard movie={movie} />
                </React.Fragment>
              );
            })
          )}
          <Box sx={{ width: "100%", marginLeft: 2, paddingTop: 2 }}>
            <Button
              variant="outlined"
              color="secondary"
              onClick={() => fetchNextPage()}
              sx={{ width: "100%" }}
            >
              MORE MOVIES
            </Button>
            <Box>
              <Typography>
                {isFetching && !isFetchingNextPage ? "Fetching..." : null}
              </Typography>
            </Box>
          </Box>
        </Grid>
        <Box>
          <Divider />
          <FeaturedSection />
        </Box>
      </Stack>
    </Layout>
  );
};

export default Movies;
