import React, { useState, useEffect } from "react";
import { useQuery, useInfiniteQuery } from "react-query";
import Container from "@mui/material/Container";
import Stack from "@mui/material/Stack";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import Divider from "@mui/material/Divider";

import Layout from "@/components/Layout";
import TvShowCard from "@/components/tv/TvShowCard";
import FeaturedSection from "@/components/home/FeaturedSection";
import { fetchAiringTodayShows } from "@/api/api";

interface Show {
  id: number;
  poster_path: string;
  name: string;
  overview: string;
}

interface LinkTabProps {
  label?: string;
  href?: string;
}

const LinkTab = (props: LinkTabProps) => {
  return (
    <Tab
      component="a"
      onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        // event.preventDefault();
      }}
      {...props}
    />
  );
};

const AiringToday = () => {
  const [page, setPage] = useState(1);

  const [value, setValue] = React.useState(0);
  const [value2, setValue2] = React.useState(1);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const handleChange2 = (event: React.SyntheticEvent, newValue: number) => {
    setValue2(newValue);
  };

  const {
    data: tvShows,
    isLoading,
    isError,
    isPreviousData,
    isSuccess,
    error,
    isFetching,
    fetchNextPage,
    isFetchingNextPage,
  } = useInfiniteQuery(["tv_shows_airing_today", page], fetchAiringTodayShows, {
    keepPreviousData: true,
    getNextPageParam: (lastPage, allPages) => {
      const maxPages = lastPage.total_pages;
      const nextPage = allPages.length + 1;
      return nextPage <= maxPages ? nextPage : undefined;
    },
  });

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }

  return (
    <Layout>
      <Box sx={{ marginBottom: 2 }}>
        <Tabs
          sx={{ backgroundColor: "#555" }}
          value={value}
          onChange={handleChange}
          indicatorColor="secondary"
          textColor="secondary"
        >
          <LinkTab label="TV Shows" href="/tv" />
          <LinkTab label="Movies" href="/movie" />
        </Tabs>
        <Container sx={{ backgroundColor: "#FFF" }}>
          <Tabs
            value={value2}
            onChange={handleChange2}
            indicatorColor="secondary"
            textColor="secondary"
          >
            <LinkTab label="Popular" href="/tv" />
            <LinkTab label="Airing Today" href="/tv/airing-today" />
            <LinkTab label="On TV" href="/tv/on-tv" />
            <LinkTab label="Top Rated" href="/tv/top-rated" />
          </Tabs>
        </Container>
      </Box>
      <Stack direction="row" spacing={5}>
        <Grid container spacing={2} md={7}>
          {tvShows?.pages.map((group, i) =>
            group.results.map((show: Show) => {
              return (
                <React.Fragment key={show.id}>
                  <TvShowCard show={show} />
                </React.Fragment>
              );
            })
          )}
          <Box sx={{ width: "100%", marginLeft: 2, paddingTop: 2 }}>
            <Button
              variant="outlined"
              color="secondary"
              onClick={() => fetchNextPage()}
              sx={{ width: "100%" }}
            >
              MORE SHOWS
            </Button>
            <Box>
              <Typography>
                {isFetching && !isFetchingNextPage ? "Fetching..." : null}
              </Typography>
            </Box>
          </Box>
        </Grid>
        <Box>
          <Divider />
          <FeaturedSection />
        </Box>
      </Stack>
    </Layout>
  );
};

export default AiringToday;
