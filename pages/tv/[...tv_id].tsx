import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import { useRouter } from "next/router";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";

import Layout from "@/components/Layout";
import ReviewCard from "@/components/ReviewCard";
import MoreInfo from "@/components/MoreInfo";
import ShowDetails from "@/components/tv/ShowDetails";
import TvAccordian from "@/components/tv/TvAccordian";
import { fetchShowDetails } from "@/api/api";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

interface Season {
  season_number: number;
}

const ShowInfoPage = () => {
  const router = useRouter();

  const { tv_id } = router.query;

  const [value, setValue] = React.useState(0);
  const [value2, setValue2] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const handleChange2 = (event: React.SyntheticEvent, newValue: number) => {
    setValue2(newValue);
  };

  const {
    data: showDetails,
    isSuccess,
    isLoading,
    isError,
  } = useQuery(["tv_show", tv_id], fetchShowDetails, {
    enabled: tv_id !== undefined,
  });

  const TabPanel = (props: TabPanelProps) => {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  };

  const a11yProps = (index: number) => {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  };

  if (isLoading) {
    return <></>;
  }
  if (isError) {
    return <div>Error</div>;
  }

  if (isSuccess) {
    return (
      <Layout>
        <Container>
          <Typography variant="h6" component="h1" sx={{ py: 2 }}>
            {showDetails.name}
          </Typography>
        </Container>

        <Stack
          direction="row"
          spacing={2}
          sx={{
            display: "flex",
            width: "100%",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Stack direction="column" spacing={2}>
            <Container>
              <Tabs
                sx={{ backgroundColor: "#555" }}
                value={value}
                onChange={handleChange}
                indicatorColor="secondary"
                textColor="secondary"
              >
                <Tab label="Videos" {...a11yProps(0)} />
                <Tab label="Reviews" {...a11yProps(1)} />
                <Tab label="Discussions" {...a11yProps(2)} />
                <Tab label="More Info" {...a11yProps(3)} />
              </Tabs>
              {value === 0 && (
                <Container sx={{ backgroundColor: "#FFF" }}>
                  <Tabs
                    sx={{ backgroundColor: "#FFF" }}
                    value={value2}
                    onChange={handleChange2}
                    indicatorColor="secondary"
                    textColor="secondary"
                  >
                    <Tab label="Newest" {...a11yProps(0)} />
                    <Tab label="Oldest" {...a11yProps(1)} />
                  </Tabs>
                </Container>
              )}
            </Container>

            <TabPanel value={value} index={0}>
              {isSuccess &&
                showDetails.seasons
                  .sort((a: any, b: any) =>
                    value2 === 0
                      ? b.season_number - a.season_number
                      : a.season_number - b.season_number
                  )
                  .map((season: Season) => {
                    return (
                      <Box sx={{ marginY: 2 }}>
                        <TvAccordian
                          season={season}
                          showDetails={showDetails}
                          sortValue={value2}
                        />
                      </Box>
                    );
                  })}
            </TabPanel>
            <TabPanel value={value} index={1}>
              <Box
                sx={{
                  marginY: 2,
                  width: "750px",
                  minWidth: "100%",
                }}
              >
                <ReviewCard id={tv_id} type="tv" />
              </Box>
            </TabPanel>
            <TabPanel value={value} index={2}>
              <Box
                sx={{
                  marginY: 2,
                  width: "750px",
                  minWidth: "100%",
                }}
              >
                <Typography variant="h6">Discussions</Typography>
              </Box>
            </TabPanel>
            <TabPanel value={value} index={3}>
              <Box sx={{ minWidth: "740px" }}>
                <MoreInfo id={tv_id} type="tv" />
              </Box>
            </TabPanel>
          </Stack>

          <Box>
            <ShowDetails showDetails={showDetails} />
          </Box>
        </Stack>
      </Layout>
    );
  }

  return <></>;
};

export default ShowInfoPage;
