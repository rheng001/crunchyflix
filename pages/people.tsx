import React, { useState } from "react";
import { useQuery } from "react-query";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import Container from "@mui/material/Container";
import Pagination from "@mui/material/Pagination";

import PeopleCard from "@/components/people/PeopleCard";
import Layout from "@/components/Layout";
import { fetchPeople } from "@/api/api";

interface Person {
  id: number;
  profile_path: string;
  name: string;
}

const PeoplePage = () => {
  const [page, setPage] = useState(1);

  const {
    data: people,
    isLoading,
    isError,
  } = useQuery(["people", page], fetchPeople, {
    keepPreviousData: true,
  });

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  if (isLoading) {
    return <></>;
  }

  if (isError) {
    return <div>Error</div>;
  }

  return (
    <Layout>
      <Typography variant={"h1"} sx={{ py: 2 }}>
        Popular People
      </Typography>
      <Grid container xs={12} spacing={4}>
        {people.results.map((person: Person) => {
          return (
            <React.Fragment key={person.id}>
              <PeopleCard person={person} />
            </React.Fragment>
          );
        })}
      </Grid>
      <Container maxWidth={"xs"} sx={{ my: 2 }}>
        <Pagination
          count={people.total_pages}
          variant="outlined"
          shape="rounded"
          onChange={handleChange}
        />
      </Container>
    </Layout>
  );
};

export default PeoplePage;
