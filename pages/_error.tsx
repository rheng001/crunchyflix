import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";

import Cat404 from "@/components/Cat404";

const ErrorPage = () => {
  return (
    <Container
      maxWidth="md"
      sx={{
        display: "flex",
        margin: "6rem auto",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Box sx={{ width: "130px", height: "188px" }}>
        <Cat404 />
      </Box>

      <Box sx={{ marginLeft: 2, maxWidth: "35rem" }}>
        <Typography
          variant="h6"
          component={"p"}
          sx={{ fontSize: "2em", fontWeight: "bold", color: "gray" }}
        >
          {`We have over `}
          <Box
            component="span"
            sx={{ fontSize: "2em", fontWeight: "bold", color: "gray" }}
          >
            404
          </Box>
          {" pages"}
        </Typography>
        <Typography
          variant="body1"
          sx={{
            marginBlockStart: "1em",
            marginBlockEnd: "2em",
            maxWidth: "420px",
            color: "#A09895",
          }}
        >
          {`but we were unable to find the page you were looking for. Please
          return to the `}
          <Link href={"/"} sx={{ color: "#E50914" }}>
            Crunchyflix homepage
          </Link>
          {"."}
        </Typography>
      </Box>
    </Container>
  );
};

export default ErrorPage;
