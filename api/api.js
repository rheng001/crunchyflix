/////////////////////////////////   MOVIES   /////////////////////////////////////////

export const fetchMovieDetails = async ({ queryKey }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/movie/${queryKey[1]}?api_key=${process.env.TMDB_API_KEY}&language=en-US`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchMovieMedia = async ({ queryKey }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/movie/${queryKey[1]}/videos?api_key=${process.env.TMDB_API_KEY}&language=en-US`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchPopularMovies = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/movie/popular?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${pageParam}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchUpcomingMovies = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/movie/upcoming?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${pageParam}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchTopRatedMovies = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/movie/top_rated?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${pageParam}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchNowPlayingMovies = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/movie/now_playing?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${pageParam}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchLatestMovies = async () => {
  const response = await fetch(
    `https://api.themoviedb.org/3/trending/tv/day?api_key=${process.env.TMDB_API_KEY}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

/////////////////////////////////   TV SHOWS    /////////////////////////////////////////

export const fetchShowDetails = async ({ queryKey }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/tv/${queryKey[1]}?api_key=${process.env.TMDB_API_KEY}&language=en-US`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchTVSeasons = async ({ queryKey }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/tv/${queryKey[1]}/season/${queryKey[2]}?api_key=${process.env.TMDB_API_KEY}&language=en-US`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchPopularTvShows = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/tv/popular?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${pageParam}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchAiringTodayShows = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/tv/airing_today?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${pageParam}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchOnTvShows = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/tv/on_the_air?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${pageParam}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchTopRatedShows = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/tv/top_rated?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${pageParam}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

/////////////////////////////////   PEOPLE    /////////////////////////////////////////

export const fetchPeople = async ({ queryKey }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/person/popular?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${queryKey[1]}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

// GENERAL

export const fetchReviews = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/${queryKey[1]}/${queryKey[2]}/reviews?api_key=${process.env.TMDB_API_KEY}&language=en-US&page=${pageParam}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchCast = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    queryKey[2] === "tv"
      ? `https://api.themoviedb.org/3/tv/${queryKey[1]}/aggregate_credits?api_key=${process.env.TMDB_API_KEY}&language=en-US`
      : `https://api.themoviedb.org/3/movie/${queryKey[1]}/credits?api_key=${process.env.TMDB_API_KEY}&language=en-US`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};

export const fetchSearchTerm = async ({ queryKey, pageParam = 1 }) => {
  const response = await fetch(
    `https://api.themoviedb.org/3/search/multi?api_key=${process.env.TMDB_API_KEY}&language=en-US&query=${queryKey[1]}&page=${queryKey[2]}&include_adult=false`
  );

  if (!response.ok) {
    throw new Error("Something went wrong.");
  }

  return response.json();
};
